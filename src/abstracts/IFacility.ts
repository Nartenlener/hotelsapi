interface IFacility {
    id: string
    name: string
}

export default IFacility