import ILocation from "./ILocation";
import IFacility from "./IFacility";
import IPhoto from "./IPhoto";


interface IHotel {
    id: string
    name: string
    stars: number
    price: number
    currency: string
    location: ILocation
    facilities: IFacility[]
    photos: IPhoto[]
}

export default IHotel