interface ILocation {
    id: string
    country: string
    district: string | null
    city: string
    postcode: string
    street: string
    housNumber: string
    latitude: number
    longitude: number
}

export default ILocation