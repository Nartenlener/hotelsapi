interface IPhoto {
    id: string
    base64: string
}

export default IPhoto