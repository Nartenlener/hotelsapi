import bodyParser from "body-parser";
import { Application } from "express";
import { BaseController } from './controllers/BaseController';
import "reflect-metadata"
import { createConnection } from 'typeorm';

class App {
    public app: Application
    public port: string

    constructor(expressServer: Application, controllers: BaseController[], port: string) {
        this.app = expressServer
        this.port = port

        this.initializeMiddlewares()
        this.initializeControllers(controllers)
        this.initializeMySql()
    }

    private initializeMiddlewares() {
        this.app.use(bodyParser.json({limit: '10mb'}))
        this.app.use(bodyParser.urlencoded({limit: '10mb', extended: false}))
    }

    private initializeControllers(controllers: BaseController[]) {
        controllers.forEach(ctr => {
            this.app.use('', ctr.router)
        })
    }

    private async initializeMySql() {
        console.log("create connection")
        await createConnection()

        //await createConnection({
        //    type: "mysql",
        //    host: "sql224.main-hosting.eu",
        //    port: 3306,
        //    username: "u273735050_aaa",
        //    password: "Boo0geev",
        //    database: "u273735050_api",
        //    synchronize: true,
        //    logging: false,
        //    entities: [
        //        "src/entity/**/*.ts"
        //    ],
        //    migrations: [
        //        "src/migration/**/*.ts"
        //    ],
        //    subscribers: [
        //        "src/subscriber/**/*.ts"
        //    ],
        //    cli: {
        //        entitiesDir: "src/entity",
        //        migrationsDir: "src/migration",
        //        subscribersDir: "src/subscriber"
        //    }
        //}).then(() => {
        //        console.log("connection created")!
        //    }
        //).catch((e) => {
        //    console.log(`Problem with create connection ${e}`)!
        //    }
        //)  
    }

    public listen() {
        this.app.listen(this.port, () => {
            console.log(`App listening on port ${this.port}`)
        })
    }
}

export default App