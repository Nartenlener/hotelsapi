import { Router } from 'express'

export abstract class BaseController {

    public path: string = ""
    public router = Router()

    constructor(path: string) {
        this.path = path
    }

    abstract initializeRouters(): void
}