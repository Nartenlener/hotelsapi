import { BaseController } from './BaseController';
import { Request, Response, NextFunction } from 'express';
import Facility from '../entity/Facility';

class FacilityController extends BaseController {

    constructor(path: string) {
        super(path);

        this.initializeRouters()
    }
    initializeRouters(): void {
        this.router.post(`${this.path}/add`, this.addFacility)
        this.router.get(`${this.path}/all`, this.getFacilities)
        this.router.delete(`${this.path}/delete`, this.deleteFacility)
    }  

    async addFacility(req: Request, res: Response, next: NextFunction) {
        try {
            if(req.body.name === null) {
                return next(new Error(`Name and description columns are required!`));
            }

            var facility = new Facility(req.body.name)
            await facility.save()

            res.status(200).send('OK')

        } catch (error) {
            return next(error)
        }
    }

    async getFacilities(req: Request, res: Response, next: NextFunction) {
        try {

            var facilities = await Facility.find()
            res.status(200).send(facilities)

        } catch (error) {
            return next(error)
        }
    }

    async deleteFacility(req: Request, res: Response, next: NextFunction) {
        try {
            if(req.body.ids === null) {
                return next(new Error(`Name and description columns are required!`));
            }

            var facilitiesToRemove = await Facility.findByIds(req.body.ids)
            await Facility.remove(facilitiesToRemove)

            var facilities = await Facility.find()
            res.status(200).send(facilities)

        } catch (error) {
            return next(error)
        }
    }
}

export default FacilityController