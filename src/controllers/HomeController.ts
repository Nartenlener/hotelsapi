import { BaseController } from './BaseController';
import { Request, Response, NextFunction } from 'express';

class HomeController extends BaseController {

    constructor(path: string) {
        super(path);

        this.initializeRouters()
    }
    initializeRouters(): void {
        this.router.get(`${this.path}/`, this.getHome)
    }

    async getHome (req: Request, res: Response, next: NextFunction) {
        try {
            res.status(200).send("I'm alive ;)")
        } catch (error) {
            next(error)
        }
    }

}

export default HomeController