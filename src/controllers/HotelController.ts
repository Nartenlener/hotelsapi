import { BaseController } from './BaseController';
import { Response, Request, NextFunction } from 'express';
import Hotel from '../entity/Hotel';
import Location from '../entity/Location';
import Facility from '../entity/Facility';
import Photo from '../entity/Photo';
import { getRepository } from 'typeorm';

class HotelController extends BaseController {

    constructor(path: string) {
        super(path)
        this.initializeRouters()
        
    }

    initializeRouters(): void {
        this.router.get(`${this.path}/all`, this.getHotels)
        this.router.post(`${this.path}/add`, this.addHotel)
        this.router.delete(`${this.path}/delete`, this.deleteHotel)
    }

    async getHotels(req: Request, res: Response, next: NextFunction) {
        try {
            let hotelDb = await getRepository(Hotel)
            var hotels = await hotelDb.find({relations: ["facilities", "location", "photos"]})
            res.status(200).send({"Poland" : hotels})
        } catch (error) {
            next(error)
        }
    }  

    async addHotel(req: Request, res: Response, next: NextFunction) {
        try {
            if(req.body === null) {
                return next(new Error(`Body for this request can not be empty!`))
            } else if(req.body.location === null) {
                return next(new Error(`Location and location value can not be empty!`))
            }

            let location = new Location(
                req.body.location.country,
                req.body.location.district,
                req.body.location.city,
                req.body.location.postcode,
                req.body.location.street,
                req.body.location.housNumber,
                req.body.location.latitude,
                req.body.location.longitude,
                )
            
            let facilities = await Facility.findByIds(req.body.facilities)

            let photos = new Array<Photo>()
            req.body.photos.forEach((element: string) => {
                let photo = new Photo(element)
                
                photos.push(photo)
            });

            var hotel = new Hotel(req.body.name, req.body.stars, req.body.price, req.body.currency, location, facilities, photos)
            await hotel.save()

            res.status(200).send('OK')

        } catch (error) {
            next(error)
        }
    }

    async deleteHotel(req: Request, res: Response, next: NextFunction) {
        try {
            let hotelsToRemove = await Hotel.find({where: {id: [req.body.ids]} ,relations: ["facilities", "location", "photos"]})
            await Hotel.remove(hotelsToRemove)

            let hotels = await Hotel.find()
            res.status(200).send({"Poland" : hotels})
        } catch (error) {
            next(error)
        }
    }
}

export default HotelController