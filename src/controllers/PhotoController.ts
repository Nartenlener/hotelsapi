import { Response, Request, NextFunction, response } from 'express';
import Photo from '../entity/Photo';
import { BaseController } from './BaseController';

class PhotoController extends BaseController {

    constructor(path: string) {
        super(path);

        this.initializeRouters()
    }
    initializeRouters(): void {
        this.router.post(`${this.path}/add`, this.addPhoto)
        this.router.get(`${this.path}/all`, this.getPhotos)
        this.router.delete(`${this.path}/delete`, this.deletePhoto)
    } 

    async addPhoto(req: Request, res: Response, next: NextFunction) {
        try {

            if(req.body.base64 === null) {
                return next(new Error(`Base64 value is required!`));
            }

            var photo = new Photo(req.body.base64)
            await photo.save()

            res.status(200).send('OK')
        } catch (error) {
        next(error)
       }
    }

    async getPhotos(req: Request, res: Response, next: NextFunction) {
        try {
            var photos = await Photo.find()

            res.status(200).send(photos)
        } catch (error) {
            next(error)
        }
    }

    async deletePhoto(req: Request, res: Response, next: NextFunction) {
        try {

            if(req.body.ids === null) {
                return next(new Error(`You have to specify which photos to delete. Add "ids" string array in body`));
            }

            var photosToDelete = await Photo.findByIds(req.body.ids)
            await Photo.remove(photosToDelete)

            var photos = await Photo.find()
            res.status(200).send(photos)
        } catch (error) {
            next(error)
        }
    }
}

export default PhotoController