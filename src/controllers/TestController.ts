import { Request, Response, NextFunction } from 'express';
import { BaseController } from './BaseController';


class TestController extends BaseController {

    constructor(path: string) {
        super(path)
        this.initializeRouters()
    }

    initializeRouters(): void {
        this.router.get(`${this.path}/hello`, this.getHello)
    }

    async getHello(req: Request, res: Response, next: NextFunction) {
        try {
            res.status(200).send("Hello World")
        } catch(error) {
            next(error)
        }
    }
}

export default TestController