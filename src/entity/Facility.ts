import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, Unique } from "typeorm"
import IFacility from '../abstracts/IFacility';

@Entity({name: "Facilities"})
@Unique(["name"])
class Facility extends BaseEntity implements IFacility {
    @PrimaryGeneratedColumn("uuid", {name: "Id"})
    id!: string

    @Column({name: "name"})
    name!: string;

    constructor(name: string) {
        super();
        
        this.name = name
    }

    add() {
        this.save();
    }
}

export default Facility