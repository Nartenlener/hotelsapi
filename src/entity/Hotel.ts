import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToMany, JoinTable, OneToMany, Unique } from 'typeorm';
import {Min, Max} from "class-validator"

import IFacility from '../abstracts/IFacility';
import IHotel from '../abstracts/IHotel';
import ILocation from '../abstracts/ILocation';
import IPhoto from '../abstracts/IPhoto';
import Location from './Location';
import Facility from './Facility';
import Photo from './Photo';

@Entity({name: "Hotels"})
@Unique(["name"])
class Hotel extends BaseEntity implements IHotel {

    @PrimaryGeneratedColumn("uuid")
    id!: string

    @Column()
    name: string

    @Column()
    @Min(1)
    @Max(5)
    stars: number

    @Column()
    price: number

    @Column()
    currency: string

    @OneToOne(() => Location, location => location.hotel, { cascade: true })
    location: ILocation

    @ManyToMany(() => Facility, { cascade: ['insert', 'update'] })
    @JoinTable({name: "HotelsFacilities"})
    @JoinColumn()
    facilities: IFacility[]

    @OneToMany(() => Photo, photo => photo.hotel, { cascade: true })    
    photos: IPhoto[]

    constructor(name: string, stars: number, price: number, currency: string, location: ILocation, facilities: IFacility[], photos: IPhoto[]) {
        super();

        this.name = name
        this.stars = stars
        this.price = price
        this.currency = currency
        this.location = location
        this.facilities = facilities
        this.photos = photos
    }
}

export default Hotel