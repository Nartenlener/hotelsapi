import { Matches } from "class-validator";
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, Unique, OneToOne, JoinColumn } from "typeorm"

import ILocation from '../abstracts/ILocation';
import Hotel from "./Hotel";
import IHotel from '../abstracts/IHotel';

@Entity({name: "Locations"})
@Unique(["country", "district", "city", "postcode", "street", "housNumber"])
class Location extends BaseEntity implements ILocation {
    @PrimaryGeneratedColumn('uuid')
    id!: string

    @Column()
    country: string;

    @Column()
    district: string;

    @Column()
    city: string;

    @Column("varchar", {length: 6})
    @Matches("/\d{2}-\d{3}/")
    postcode: string;

    @Column()
    street: string;

    @Column("varchar", {length: 10})
    housNumber: string;

    @Column({type: 'decimal', precision: 17, scale: 14})
    latitude: number;

    @Column({type: 'decimal', precision: 17, scale: 14})
    longitude: number;

    @OneToOne(() => Hotel, hotel => hotel.location, {onDelete: 'CASCADE'})
    @JoinColumn()
    hotel!: IHotel;

    constructor(country: string, district: string, city: string, postcode: string, street: string, housNumber: string, latitude: number, longitude: number) {
        super()

        this.country = country
        this.district = district
        this.city = city
        this.postcode = postcode
        this.street = street
        this.housNumber = housNumber
        this.latitude = latitude
        this.longitude = longitude
    }

}

export default Location