import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import IPhoto from "../abstracts/IPhoto";
import IHotel from '../abstracts/IHotel';
import Hotel from './Hotel';

@Entity({name: "Photos"})
class Photo extends BaseEntity implements IPhoto {
    @PrimaryGeneratedColumn("uuid")
    id!: string

    @Column({ type: "longtext" })
    base64: string

    @ManyToOne(() => Hotel, hotel => hotel.photos, { onDelete: "CASCADE" })
    @JoinColumn()
    hotel!: IHotel

    constructor(base64: string) {
        super()

        this.base64 = base64
    }

}

export default Photo