import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from "typeorm"

@Entity({name: "TabelkTestowa"})
class Test extends BaseEntity {

    @PrimaryGeneratedColumn("uuid")
    id!: string

    @Column({name: "Nazwa"})
    name!: string
}

export default Test