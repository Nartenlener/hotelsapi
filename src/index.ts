import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import expressServer from "./server";

/*
    Jak połączyć expressjs z funkcjami firebase
    https://github.com/jthegedus/firebase-gcp-examples/tree/main/functions-express/src
*/

admin.initializeApp();
exports.api = functions.https.onRequest(expressServer);
