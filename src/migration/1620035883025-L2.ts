import {MigrationInterface, QueryRunner} from "typeorm";

export class L21620035883025 implements MigrationInterface {
    name = 'L21620035883025'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `Locations` DROP FOREIGN KEY `FK_474f039772ce77415a9b8e885fc`");
        await queryRunner.query("ALTER TABLE `Locations` CHANGE `hotelIdId` `hotelIdId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `Photos` DROP FOREIGN KEY `FK_7bb17386575de79576862f4ec89`");
        await queryRunner.query("ALTER TABLE `Photos` CHANGE `hotelIdId` `hotelIdId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `Locations` ADD CONSTRAINT `FK_474f039772ce77415a9b8e885fc` FOREIGN KEY (`hotelIdId`) REFERENCES `Hotels`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `Photos` ADD CONSTRAINT `FK_7bb17386575de79576862f4ec89` FOREIGN KEY (`hotelIdId`) REFERENCES `Hotels`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `Photos` DROP FOREIGN KEY `FK_7bb17386575de79576862f4ec89`");
        await queryRunner.query("ALTER TABLE `Locations` DROP FOREIGN KEY `FK_474f039772ce77415a9b8e885fc`");
        await queryRunner.query("ALTER TABLE `Photos` CHANGE `hotelIdId` `hotelIdId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `Photos` ADD CONSTRAINT `FK_7bb17386575de79576862f4ec89` FOREIGN KEY (`hotelIdId`) REFERENCES `Hotels`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `Locations` CHANGE `hotelIdId` `hotelIdId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `Locations` ADD CONSTRAINT `FK_474f039772ce77415a9b8e885fc` FOREIGN KEY (`hotelIdId`) REFERENCES `Hotels`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

}
