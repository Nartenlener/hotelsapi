import {MigrationInterface, QueryRunner} from "typeorm";

export class Refactoring1620036025836 implements MigrationInterface {
    name = 'Refactoring1620036025836'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP INDEX `IDX_9b92240ba1ee41f56e7c94958b` ON `Locations`");
        await queryRunner.query("ALTER TABLE `Locations` DROP FOREIGN KEY `FK_9b92240ba1ee41f56e7c94958be`");
        await queryRunner.query("ALTER TABLE `Locations` CHANGE `hotelId` `hotelId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `Photos` DROP FOREIGN KEY `FK_5d4b66e0ec35bd7cc5f486de951`");
        await queryRunner.query("ALTER TABLE `Photos` CHANGE `hotelId` `hotelId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `Locations` ADD CONSTRAINT `FK_9b92240ba1ee41f56e7c94958be` FOREIGN KEY (`hotelId`) REFERENCES `Hotels`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `Photos` ADD CONSTRAINT `FK_5d4b66e0ec35bd7cc5f486de951` FOREIGN KEY (`hotelId`) REFERENCES `Hotels`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `Photos` DROP FOREIGN KEY `FK_5d4b66e0ec35bd7cc5f486de951`");
        await queryRunner.query("ALTER TABLE `Locations` DROP FOREIGN KEY `FK_9b92240ba1ee41f56e7c94958be`");
        await queryRunner.query("ALTER TABLE `Photos` CHANGE `hotelId` `hotelId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `Photos` ADD CONSTRAINT `FK_5d4b66e0ec35bd7cc5f486de951` FOREIGN KEY (`hotelId`) REFERENCES `Hotels`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `Locations` CHANGE `hotelId` `hotelId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `Locations` ADD CONSTRAINT `FK_9b92240ba1ee41f56e7c94958be` FOREIGN KEY (`hotelId`) REFERENCES `Hotels`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("CREATE UNIQUE INDEX `IDX_9b92240ba1ee41f56e7c94958b` ON `Locations` (`hotelId`)");
    }

}
