import express from "express";
import cors from "cors"
import App from './app';
import TestController from './controllers/TestController';
import HotelController from './controllers/HotelController';
import HomeController from './controllers/HomeController';
import FacilityController from "./controllers/FacilityController";
import PhotoController from "./controllers/PhotoController";

const api: string = '/api'

const expressServer = express()

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 
 }
 
expressServer.use(cors(corsOptions))
expressServer.use(express.json({limit: '10mb'}))
expressServer.use(express.urlencoded({limit: '10mb', extended: true}))

const app = new App (expressServer,
    [
        new HomeController(''),
        new TestController(`${api}/test`),
        new HotelController(`${api}/hotels`),
        new FacilityController(`${api}/facilties`),
        new PhotoController(`${api}/photos`)
    ],
    process.env.PORT || '8810'
)

app.listen()

export default expressServer